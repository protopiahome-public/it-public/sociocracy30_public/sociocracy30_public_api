const jose = require('node-jose');

export default async function (client) {

    const key = await jose.JWK.asKey({
        "client_id":client._id,
        "kid": client._id,
        "kty": "oct",
        "k": client.client_secret
    });

    return key;

}