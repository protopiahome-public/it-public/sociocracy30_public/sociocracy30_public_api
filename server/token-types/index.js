import idTokenType from "./IdTokenType";
import accessTokenType from "./AccessTokenType";
import refreshTokenType from "./RefreshTokenType";

module.exports = {

    idTokenType: idTokenType,
    accessTokenType: accessTokenType,
    refreshTokenType: refreshTokenType
}