import jws from "jws";
import TokenType from "./TokenType";

//Stateless - Сохраняющий свое состояние во внешний источник.


/**
 * Generate a JWT
 *
 * @param $privateKey The private key to use to sign the token
 * @param $iss The Issuer, usually the URI for the oauth server
 * @param $sub The Subject, usually a user_id
 * @param $aud The Audience, usually client_id
 * @param $exp The Expiration date. If the current time is greater than the exp, the JWT is invalid
 * @param $iat The "Issued at" date.
 * @param $nbf The "not before" time. If the current time is less than the nbf, the JWT is invalid
 * @param $jti The "jwt token identifier", or nonce for this JWT
 *
 * @return string
 */
const url = "https://ecosystem.protopia.com";

export default class RefreshTokenType extends TokenType {

    async createToken(server, client, user = {}, scope = {})  {

        this.lifeTime = 60 * 60 * 24 * 14;  // 2 weeks.
        const issued_at = Date.now();
        const expires_at = issued_at+this.lifeTime;


        const sub = user ? user._id : client._id;

        this.json = {
            iss: server.url,
            sub: sub,
            aud: url,
            iat: issued_at,
            exp: expires_at,

            client_id: client._id,
            scp: scope
        };

        const string = await JSON.stringify(this.json);
        this.payload = await Buffer.from(string);

    }

    async createSignToken(server, client, user = {}, scope = {}){
        await this.createToken(server, client, user , scope );

        return await this.sign ();
    }

    async createCryptToken(server, client, user = {}, scope = {}){
        await this.createToken(server, client, user , scope );

        return await this.encrypt();
    }

}

//шифруется ключом авторизационного сервера

// //https://dajbych.net/stateless-stateful-or-actor-service