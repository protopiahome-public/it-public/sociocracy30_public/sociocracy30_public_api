const { query } = require('nact');

export default async function (msg,  ctx) {

    const sendCodeActor = ctx.children.get("send_code");
    const collectionItemActor = ctx.children.get("item");

    //						query(sayActor, {
    // 							"api_uri": client.api_uri,
    // 							"external_id": receiver.external_id,
    // 							"external_type": receiver.external_type,
    // 							"message": msg.input.chat_title + ":\n" + msg.input.post_text + "\n\n" + post_link
    // 						}, global.actor_timeout);

    await query(collectionItemActor, {type: "associate_session",
        input: {
            "client_id": msg.client_id,
            "user_id": msg.user_id,
            "oob_channel":"telegram",
            "telegram_id": msg.associate_hint,
            "user_code": msg.user_code
        }
    }, global.actor_timeout);

    dispatch(sendCodeActor, {
        "client_id": msg.client_id,
        "user_id": msg.user_id,
        "oob_channel":"slack",
        "slack_id": msg.associate_hint,
        "user_code": msg.user_code
    }, ctx.self );

}