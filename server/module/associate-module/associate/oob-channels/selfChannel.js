const { query } = require('nact');

export default async function (msg, ctx) {

    const collectionItemActor = ctx.children.get("item");

    await query(collectionItemActor, {type: "associate_session",
        input: {
            "client_id": msg.client_id,
            "user_id": msg.user_id,
            "user_code": msg.user_code,
            "for_channel": msg.for_channel
        }
    }, global.actor_timeout);

}