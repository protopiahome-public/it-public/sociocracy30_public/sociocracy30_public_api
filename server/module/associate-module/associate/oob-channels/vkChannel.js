const { query, dispatch } = require('nact');

export default async function (msg, ctx) {

    const sendCodeActor = ctx.children.get("send_code");
    const collectionItemActor = ctx.children.get("item");

    await query(collectionItemActor, {type: "associate_session",
        input: {
            "client_id": msg.client_id,
            "user_id": msg.user_id,
            "oob_channel":"vk",
            "vk_id": msg.associate_hint,
            "user_code": msg.user_code
        }
    }, global.actor_timeout);


    dispatch(sendCodeActor, {
        "client_id": msg.client_id,
        "user_id": msg.user_id,
        "oob_channel":"slack",
        "slack_id": msg.associate_hint,
        "user_code": msg.user_code
    }, ctx.self );

}