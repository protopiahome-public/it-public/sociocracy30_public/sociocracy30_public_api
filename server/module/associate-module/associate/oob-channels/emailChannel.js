const { query } = require('nact');
var nodemailer = require('nodemailer');

export default async function (msg, ctx) {

    const client_id = msg.client_id;
    const    user_id = msg.user_id;
    const    email = msg.associate_hint;
    const    user_code = msg.user_code;

    // if(!user.email_verified){}

    const collectionItemActor = ctx.children.get("item");

    // create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport('smtps://user%40gmail.com:pass@smtp.gmail.com');

// setup e-mail data with unicode symbols
    var mailOptions = {
        from: '"Protopia" <protopia@gmail.com>', // sender address
        to: email, // list of receivers
        subject: 'One-Time Password', // Subject line
        text: 'password: ' + user_code, // plaintext body
        html: '<b>password: '+user_code+'</b>' // html body
    };

// send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            return console.log(error);
        }
        console.log('Message sent: ' + info.response);

    });

    await query(collectionItemActor, {type: "associate_session",
        input: {
            "email": email,
            "user_code": user_code
        }
    }, global.actor_timeout);

}





