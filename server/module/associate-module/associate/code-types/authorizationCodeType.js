import authenticator from 'otplib/authenticator';

const secret = authenticator.generateSecret();

// authorizationCodeLifetime: 5 * 60   // 5 minutes.

module.exports = {
    generate: async ()=>{
        return authenticator.generate(secret);

    },
    verify: async (token)=> {
        try {
            return authenticator.verify({token, secret});
        } catch (err) {

            console.error(err);
        }

    }
}