import authenticator from 'otplib/authenticator';

const secret = authenticator.generateSecret();

module.exports = {
    generate: async ()=>{
        return authenticator.generate(secret);

    },
    verify: async (token)=> {
        try {
            return authenticator.verify({token, secret});
        } catch (err) {

            console.error(err);
        }

    }
}

// authorizationCodeLifetime: 5 * 60   // 5 minutes.

// https://www.npmjs.com/package/otplib