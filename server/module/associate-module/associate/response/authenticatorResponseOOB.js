export default async function (authenticator_type,  oob_channel) {

    return {
        authenticator_type: authenticator_type,
        oob_channel: oob_channel

    }

}