const {AuthenticationError, ForbiddenError} = require('apollo-server');
import {ObjectId} from "promised-mongo";

const { query } = require('nact');

const resource = "post"||"proposal";

module.exports = {

    Mutation: {
        changeChat: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");

            if (args.id) {
                return await query(collectionItemActor, {
                    "type": "chat",
                    search: {_id: args.id},
                    input: args.input
                }, global.actor_timeout);
            } else {
                return await query(collectionItemActor, {"type": "chat", input: args.input}, global.actor_timeout);
            }

        },
        changeProposal: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");

            if (args.id) {
                return await query(collectionItemActor, {
                    "type": "proposal",
                    search: {_id: args.id},
                    input: args.input
                }, global.actor_timeout);
            } else {
                return await query(collectionItemActor, {"type": "proposal", input: args.input}, global.actor_timeout);
            }

        },
        changeMyProposal: async (obj, args, ctx, info) => {

            let user = await ctx.user;

            const collectionItemActor = ctx.children.get("item");

            if (args.id) {
                // добавить условия про my args.input.author_id = new ObjectId(user.id);
                return await query(collectionItemActor, {
                    "type": "proposal",
                    search: {_id: args.id},
                    input: args.input
                }, global.actor_timeout);
            } else {
                args.input.date = new Date();
                args.input.author_id = new ObjectId(user._id);
                return await query(collectionItemActor, {"type": "proposal", input: args.input}, global.actor_timeout);
            }

        },
		voteProposal: async (obj, args, ctx, info) => {
			
			let vote = await ctx.db.vote.insert({
					proposal_id: args.proposal_id,
					type: args.type,
					author_id: ctx.user._id,
					date: new Date(),
				});
				
			let proposal = await ctx.db.proposal.findAndModify({
				query: { _id: new ObjectId(args.proposal_id) },
				update: {
					$push: {votes_ids : new ObjectId(vote._id)
					}
				}
			});

			return proposal;

		},

    },
    Query: {
		getProposal: async (obj, args, ctx, info) => {

			let user = await ctx.user;

			  

			  
			
			let proposals = await ctx.db.proposal.aggregate(
				[
					{"$match": {_id: new ObjectId(args.id)}},
					{"$sort": {"date": -1 }},
					
				]);

			return proposals[0];

		},

		getProposals: async (obj, args, ctx, info) => {

			let user = await ctx.user;

			  

			  
			
			let proposals = await ctx.db.proposal.aggregate(
				[
					{"$sort": {"date": -1 }},
					
				]);

			
			return proposals;
			
		},
		
		getProposalsForChat: async (obj, args, ctx, info) => {

			let user = await ctx.user;

			  

			  
			
			let proposals = await ctx.db.proposal.aggregate(
				[
					{"$match": {chat_id: args.chat_id}},
					{"$sort": {"date": -1 }},
					
				]);

			
			return proposals;
			
		},

		getChat: async (obj, args, ctx, info) => {

			let user = await ctx.user;

			  

			  
			
			let chats = await ctx.db.chat.aggregate(
				[
					{"$match": {_id: new ObjectId(args.id)}},
					{"$sort": {"date": -1 }},
					
				]);

			return chats[0];

		},

		getChats: async (obj, args, ctx, info) => {

			let user = await ctx.user;

			  

			  
			
			let chats = await ctx.db.chat.aggregate(
				[
					{"$sort": {"date": -1 }},
					
				]);

			
			return chats;
			
		},

		getChatByExternal: async (obj, args, ctx, info) => {

			let user = await ctx.user;

			  

			  
			
			let chats = await ctx.db.chat.aggregate(
				[
					{"$match": args.input},
					{"$sort": {"date": -1 }},
					
				]);

			return chats[0];

		},
	},
	Proposal : {
		author:  async (obj, args, ctx, info) => {
			return (await ctx.db.user.find({_id: new ObjectId(obj.author_id)}))[0];
		},
		votes:  async (obj, args, ctx, info) => {
			if (obj.votes_ids) {
				return await ctx.db.vote.findAsCursor({_id: { $in : obj.votes_ids }}).sort({date: -1}).toArray();
			}
		}
	},
	Vote : {
		author:  async (obj, args, ctx, info) => {
			return (await ctx.db.user.find({_id: new ObjectId(obj.author_id)}))[0];
		},
	}
}