import {DateTimeResolver} from 'graphql-scalars';

import bcrypt from "bcrypt";

const { query } = require('nact');

const resource = "user";

module.exports = {

    DateTime: DateTimeResolver,
    Query: {

        userInfo: async (obj, args, ctx, info) => {

            return ctx.user;

        },

        getCurrentUser: async (obj, args, ctx, info) => {

            let user = await ctx.user;



            // const {collectionItemActor} = require("../../actors/actorSystem");

            const collectionItemActor = ctx.children.get("users");
            return (await query(collectionItemActor, {"type": "user", search: {_id: user._id}}, global.actor_timeout))[0];

        },

        getUser: async (obj, args, ctx, info) => {

            // const {collectionItemActor} = require("../../actors/actorSystem");

            const collectionItemActor = ctx.children.get("users");
            return (await query(collectionItemActor, {search: {_id: args._id}}, global.actor_timeout))[0];

        },

        getUsers: async (obj, args, ctx, info) => {

            // const {collectionActor} = require("../../actors/actorSystem");


            const collectionActor = ctx.children.get("users");

            return await query(collectionActor, {}, global.actor_timeout);

        },


        getUserByExternalId: async (obj, args, ctx, info) => {

            // const {collectionItemActor}  = require("../../actors/actorSystem");
            const collectionItemActor = ctx.children.get("users");

            args.external_id = parseInt(args.external_id);

            let user;
            switch (args.external_system) {
                case "telegram":
                    user = await query(collectionItemActor, {search:
                            {telegram_id: args.external_id}
                    }, global.actor_timeout)
                    break;
                case "slack":
                    user = await query(collectionItemActor, {search:
                            {slack_id: args.external_id}
                    }, global.actor_timeout);
                    break;
                case "vk":
                    user = await query(collectionItemActor, {search:
                            {vk_id: args.external_id}
                    }, global.actor_timeout);
                    break;

            }

            return user[0];

        },

    },

    Mutation:{

        registerUser: async (obj, args, ctx, info) => {

            // const { collectionItemActor} = require("../../actors/actorSystem");
            const collectionItemActor = ctx.children.get("item");

            if(args.input.password){
                const crypto_password = bcrypt.hashSync(args.input.password, 10);
                args.input.crypto_password = crypto_password;
                delete (args.input.password);
            }

            args.input.roles = ['user'];

            return await query(collectionItemActor, {type: "user", input: args.input}, global.actor_timeout);

        },

        changeUser: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");
            // const { collectionItemActor} = require("../../actors/actorSystem");

            return await query(collectionItemActor,  {type: "user", search:{_id: args._id}, input: args.input }, global.actor_timeout);


        },

        changeCurrentUser: async (obj, args, ctx, info) => {

            let client = await ctx.client;
            let current_user = await ctx.user;

            const collectionItemActor = ctx.children.get("item");

            switch (client.application_type) {
                case "leaderid":
                    // args.input
                    break;
                case "telegram":
                    //args.input
                    break;
                case "vk":
                    //args.input
                    break;
                case "slack":
                    //args.input
                    break;
                case "email":
                    //args.input
                    break;
                default:
                    break;
            }


            if (args.input.password) {
                let crypto_password = bcrypt.hashSync(args.input.password, 10);
                delete args.input.password;
                args.input.crypto_password = crypto_password;
                return await query(collectionItemActor, {type: "user", search:{_id: current_user._id}, input: args.input }, global.actor_timeout);
            }else{
                return await query(collectionItemActor, {type: "user", search:{_id: current_user._id}, input: args.input }, global.actor_timeout);
            }



        },

        changeUserRoles: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");
            // const {collectionItemActor}  = require("../../actors/actorSystem");

            return await query(collectionItemActor, {type: "user", search: {_id: args._id}, input: {roles: args.roles }  }, global.actor_timeout);

        },

        connect: async (obj, args, ctx, info) => {

            let current_user = await ctx.user;

            const collectionItemActor = ctx.children.get("item");
            // const { collectionItemActor} = require("../../actors/actorSystem");

            let authenticator =  await query(collectionItemActor, {type: "authenticate_session", search: {"user_code": args.user_code} }, global.actor_timeout);

            let user;
            switch (authenticator.oob_channel) {
                case "slack":
                    user = await query(collectionItemActor, {type: "user", search: {"slack_id": authenticator.slack_id}  }, global.actor_timeout);
                    break;
                case "telegram":
                    user = await query(collectionItemActor, {type: "user", search: {"telegram_id": authenticator.telegram_id}  }, global.actor_timeout);
                    break;
                case "vk":
                    user = await query(collectionItemActor, {type: "user", search: {"vk_id": authenticator.vk_id}  }, global.actor_timeout);
                    break;
            }

            current_user = await query(collectionItemActor, {type: "user", search: {_id: current_user._id}, input:{
                    user_ids:[user._id],
                }}, global.actor_timeout);

            user = await query(collectionItemActor, {type: "user", search: {"_id": user._id} , input:{is_remove: true} }, global.actor_timeout);

            return user;

        },

    }


}