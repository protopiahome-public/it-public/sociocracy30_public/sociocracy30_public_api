import db from "../../../db";

import {ObjectId} from "promised-mongo";

const { dispatch } = require('nact');

export default async function(msg, ctx)
{

    let users = [];
    let match;

	if (!msg.search) {
		msg.search = {};
	}
	match = msg.search;
	if (match._id) {
		match._id = new ObjectId(match._id);
	}
	
	try {
    users = await db.user.aggregate(
        [
            {"$match": match},
			{"$sort" : {"name" : 1}}
        ]);
		
		users.forEach((e)=>{
			e.title = e.name
		});
	} catch (e) {console.log(e);}
    dispatch(ctx.sender,  users, ctx.self);

}