import userCollection from "./userCollection";

const { spawnStateless} = require('nact');

export default function(serverActor){
	spawnStateless(serverActor, userCollection, "users");
}
