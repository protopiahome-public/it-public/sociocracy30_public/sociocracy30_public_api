const { query } = require('nact');

module.exports = {

    Mutation: {
        register: async (obj, args, ctx, info) => {

            // const collectionItemActor = ctx.children.get("item");
            const collectionItemActor = ctx.children.get("item");

            return await query(collectionItemActor, {"type": "client", input: args.input}, global.actor_timeout);

        },

        updateClient: async (obj, args, ctx, info) => {

            // const collectionItemActor = ctx.children.get("item");
            const collectionItemActor = ctx.children.get("item");

            return await query(collectionItemActor, {
                "type": "client",
                search: {_id: args._id},
                input: args.input
            }, global.actor_timeout);

        },

        removeClient: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");
            // const collectionItemActor = ctx.children.get("item");

            return await query(collectionItemActor, {
                "type": "client",
                search: {_id: args._id},
                input: {is_remove: true}
            }, global.actor_timeout);

        },

    },
    Query:{
        getClients: async (obj, args, ctx, info) => {


            // const {collectionActor}  = require("../../../actorSystem");
            const collectionActor = ctx.children.get("collection");



            return await query(collectionActor,  {"type": "client"}, global.actor_timeout);

        },

        getClient: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");
            // const collectionItemActor = ctx.children.get("item");

            return await query(collectionItemActor,  {"type": "client", _id: args._id}, global.actor_timeout);

        },


    }
}