import db from "../../../../db";
import {ObjectId} from "promised-mongo";


import { AccessControl } from 'role-acl';
import grants from "../../../../config/grants";

const access = new AccessControl(grants);
const { dispatch, spawn, stop } = require('nact');

//пользователь дает клиенту разрешения.
//"role", "resource", "action", "attributes"
//"role" - client.app_type or user.roles
//"resource" - server (client.app_type) (such as resources, computers, and applications)

// grants может получить как клиент, так и пользователь
// grants для клиента определяются его app_type
// grants для пользователя определяются его ролями
// пользователь может передавать через scope свои grants пользователю.

// Scopes
// A scope is the smallest entity that describes a single permission.
// Roles
// A role is a collection of multiple scopes (permissions) that can be assigned to a user or another role.

// access.grant('user').condition(
//     (context) => {
//         return ctx.user.groups.filter(value => ctx.resource.groups.includes(value))
//     }
// ).execute('read').on('theme');

//TODO пока не универсальный.

/**
 * Generate a JWT
 *
 * @param $iss The Issuer, usually the URI for the oauth server
 * @param $sub The Subject, usually a user_id
 * @param $aud The Audience, usually client URI
 * @param $exp The Expiration date. If the current time is greater than the exp, the JWT is invalid
 * @param $iat The "Issued at" date.
 * @param $nbf The "not before" time. If the current time is less than the nbf, the JWT is invalid
 * @param $jti The "jwt token identifier", or nonce for this JWT
 *
 */

//shutdownAfter: 10 * minutes


export default (parent, sessionId, initalState) => spawn(
    parent,
    async (
        state =
            {
                token:{
                    sub: "",
                    aud: "",
                    iss: "",
                    iat: "",
                    exp: "",

                    sid: "",

                    client_id: "",
                    scp: ""
                },

                client:{
                    application_type: "",
                    client_secret: "",
                },

                user:{
                    roles: "",
                    role: ""
                }

            },
        msg, ctx) =>
    {

        let token = state.token;
        let client = state.client;
        let user = state.user;

        // if(new Date.now()  > state.token.exp ){
        //
        //     stop(ctx.self);
        //     throw new AuthenticationError('Time is incorrect');
        // }

        if(!client){

            console.log(token.client_id);

            if(token.client_id){
                client = await db.client.findOne({
                    query: {_id: new ObjectId(token.client_id)}
                });

                state.client = client;

                if(!client)
                    throw new AuthenticationError('Must authenticate');

                user = await db.user.findOne({
                    query: {_id: new ObjectId(token.sub)}
                });

                state.user = user;

            }else{
                client = await db.client.findOne({
                    query: {_id: new ObjectId(token.sub)}
                });

                state.client = client;

                //user undefined
            }

        }

       let serverConfigActor;
        if(ctx.children.has("server_config")){
            serverConfigActor = ctx.children.get("server_config");
        }
        // const config = await query(serverConfigActor,  {}, global.actor_timeout);

        //|| host !== token.iss
        // if(config.uri !== token.iss ){
        //     throw new AuthenticationError('URL incorrect');
        // }
        //
        // if(config.uri !== token.aud){
        //     throw new AuthenticationError('URL incorrect');
        // }

        if(client.application_type && user.role){
            access.grant('admin').extend('user');
            access.grant(user.role).extend(client.application_type);
        }

        // const scope = token.scp;
        //not client_creditals
        // scope.forEach( (e) => {
        //     access.grant(client.app_type).execute(e).on('*')
        // } );


        const permission = await access.permission({
            role: client.application_type,
            resource: "",
            action: msg.query_name
            // , context: context
        });

        if(!permission.granted)
            throw new ForbiddenError('Resource Forbidden');

        dispatch(ctx.sender, state.token, ctx.self);
        return state;
    },
    sessionId,
    {initalState: initalState}
);

// throw new AuthorizationError('you must be logged in to query this schema');

// https://www.npmjs.com/package/role-acl
// https://onury.io/accesscontrol/?content=faq
// https://developer.mindsphere.io/concepts/concept-roles-scopes.html
// https://wso2.com/library/articles/2015/12/article-role-based-access-control-for-apis-exposed-via-wso2-api-manager-using-oauth-2.0-scopes/
// https://onury.io/accesscontrol/