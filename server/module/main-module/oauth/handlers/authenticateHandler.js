import getQueryName from "../helpers/getQueryName";
import AccessTokenType from "../../../../token-types/AccessTokenType";

import { AccessControl } from 'role-acl';
import grants from "../../../../config/grants";

const access = new AccessControl(grants);



const {AuthenticationError, ForbiddenError} = require('apollo-server');
const uuidv1 = require('uuid/v1');

var jose = require('node-jose');

import {InvalidTokenError} from "../errors";
import serverConfig from "../../actors/serverConfig";
import db from "../../../../db";
import {ObjectId} from "promised-mongo";
import createClientKey from "../../../../token-types/createClientKey";

const { query, dispatch } = require('nact');

export default async (request, h, ctx) => {

    const headers = request.headers;
    const host = request.info.host;

    const query_name = getQueryName(request);

    //refferer === client.uri.host

    //проверить при не получении запроса
    const authorization = headers.authorization || false;

    console.log("authorization: " + authorization);

    if (!authorization)
        throw new AuthenticationError('Has not Authorization Header');

    const authorizationArray = authorization.split(' ');

    //token.match(/Bearer\s(\S+)/);
    if (authorizationArray[0] !== 'Bearer')
        throw new AuthenticationError('It is not Bearer Type');

    let serverConfigActor;

    if(ctx.children.has("server_config")){
        serverConfigActor = ctx.children.get("server_config");
    }
    const config = await query(serverConfigActor,  {}, global.actor_timeout);

    let sid = uuidv1();

    let client;
    let user;

    try {
        const key = await createClientKey(config);
        const access_token = new AccessTokenType(authorizationArray[1], key);
        await access_token.decrypt();
        const token_payload = access_token.getPayloadJSON();

        // throw new AuthenticationError('Signature incorrect');

        console.log("token_payload: ");
        console.log(token_payload);

        if(token_payload.client_id !== token_payload.sub){
            client = await db.client.findOne({
                _id: new ObjectId(token_payload.client_id)
            });

            user = await db.user.findOne({
                _id: new ObjectId(token_payload.sub)
            });

            console.log(client);
            console.log(user);
        }else{
            client = await db.client.findOne({
                _id: new ObjectId(token_payload.sub)
            });

            //user undefined
            console.log(client);
        }

        if(!client)
            throw new AuthenticationError('Must authenticate');

        if (client.basic_domain_uris) {
            if (!client.basic_domain_uris.includes(new URL(headers.referer).hostname)) {
                throw new AuthenticationError('Wrong referer');
            }
        }


        // let serverConfigActor;
        // if(ctx.children.has("server_config")){
        //     serverConfigActor = ctx.children.get("server_config");
        // }
        // const config = await query(serverConfigActor,  {}, global.actor_timeout);

        //|| host !== token_payload.iss
        // if(config.uri !== token_payload.iss ){
        //     throw new AuthenticationError('URL incorrect');
        // }
        //
        // if(config.uri !== token_payload.aud){
        //     throw new AuthenticationError('URL incorrect');
        // }



        // const scope = token_payload.scp;
        //not client_creditals
        // scope.forEach( (e) => {
        //     access.grant(client.app_type).execute(e).on('*')
        // } );







        // let childActor = sessionService(ctx.self, sid, {token: token_payload});

        // sid = json.sid || "1";
        //
        // let childActor;
        // if(ctx.children.has(sid)){
        //     childActor = ctx.children.get(sid);
        // } else {
        //
        // }
        // session = await query(childActor,  {host: host, query_name: query_name}, global.actor_timeout);


        // console.log(session);
    }catch (e) {
        // console.log(e);
        throw new InvalidTokenError('Invalid Token');
    }

    if(user && user.roles && client.application_type){
        access.grant('admin').extend('user');
        access.grant(user.roles).extend(client.application_type);
    }



    const permission = await access.permission({
        role: user ? user.roles : client.application_type,
        // role: client.application_type,
        resource: "",
        action: query_name
        // ,
        // context: ""
    });

    if(!permission.granted)
        throw new ForbiddenError('Resource Forbidden');

    return {
        user: user,
        client: client
    };

}


// права метода
// if (name) {
//     h.set('X-Accepted-OAuth-Scopes', name);
// }

// Согласие пользователя this.addAuthorizedScopesHeader
// if (scope) {
//     h.set('X-OAuth-Scopes', scope);
// }