import gql from "graphql-tag";

export default function(request) {

    // console.log(request.payload);

    let name = "";

    if(request.payload.query){
        const text = gql`
            ${request.payload.query}
        `;

        name = text.definitions[0].selectionSet.selections[0].name.value
    }else if(request.payload.mutation){
        const text = gql`
            ${request.payload.mutation}
        `;

        name = text.definitions[0].selectionSet.selections[0].name.value
    }

    return name;

}
