import tokenResponse from "../response/tokenResponse";
import AccessTokenType from "../../../../token-types/AccessTokenType";
import IdTokenType from "../../../../token-types/IdTokenType";
import RefreshTokenType from "../../../../token-types/RefreshTokenType";

import {UnauthorizedClientError, UnknownAuthReqIdError} from "../../errors";
const jose = require('node-jose');
import getClientByAssertion from "../helpers/getClientByAssertion";
import createClientKey from "../../../../token-types/createClientKey";

const { query } = require('nact');


export default async function(args, ctx) {

    const {client}  = await getClientByAssertion(args.assertion, ctx);

    let serverConfigActor;
    if(ctx.children.has("server_config")){
        serverConfigActor = ctx.children.get("server_config");
    }

    const config = await query(serverConfigActor,  {}, global.actor_timeout);

    const key1 = await createClientKey(config);

    const access_token = new AccessTokenType("", key1);
    const at = await access_token.createCryptToken(config, client);
    const expires_at = await access_token.expires_in();

    const refresh_token = new RefreshTokenType("", key1);
    const rt = await refresh_token.encrypt(key1);

    return tokenResponse(at, expires_at, rt, "");

}