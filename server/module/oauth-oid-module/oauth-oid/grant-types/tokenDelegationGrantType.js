import tokenResponse from "../response/tokenResponse";
import accessTokenType from "../../../../token-types/AccessTokenType";
import refreshTokenType from "../../../../token-types/RefreshTokenType";



const {ApolloError, AuthenticationError, ForbiddenError} = require('apollo-server');

export default async function(args, ctx) {

    const client = ctx.client;
    const server_secret = ctx.server_secret;

    const collectionItemActor = ctx.children.get("item");

    let authenticator =  await query(collectionItemActor, {"type": "authenticate_session", search: {"auth_req_id": args.auth_req_id} }, global.actor_timeout);

    if (!authenticator) {
        throw new ApolloError('Incorrect password')
    }

    const now = new Date();
    const issued_at = now.getTime();

    const access_token = accessTokenType.generate(client._id, args.user._id, args.scope, issued_at, server_secret);
    const expires_at = accessTokenType.expires_in(issued_at);
    const refresh_token = refreshTokenType.generate(client._id, args.user._id, args.scope, issued_at, server_secret);


    return tokenResponse(access_token, expires_at, refresh_token, code.scope);
}