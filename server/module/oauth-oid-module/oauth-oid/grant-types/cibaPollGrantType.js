import tokenResponse from "../response/tokenResponse";
import AccessTokenType from "../../../../token-types/AccessTokenType";
import IdTokenType from "../../../../token-types/IdTokenType";
import RefreshTokenType from "../../../../token-types/RefreshTokenType";

import {UnauthorizedClientError, UnknownAuthReqIdError} from "../../errors";
import getClientByAssertion from "../helpers/getClientByAssertion";
import createClientKey from "../../../../token-types/createClientKey";

const { query } = require('nact');


export default async function(args, ctx) {

    const {client} = await getClientByAssertion(args.assertion, ctx);

    const collectionItemActor = ctx.children.get("item");

    let authenticate_session =  await query(collectionItemActor, {"type": "authenticate_session", search: {"_id": args.auth_req_id} }, global.actor_timeout);

    if (!authenticate_session) {
        throw new UnknownAuthReqIdError('Unknown auth_req_id');
    }

    let serverConfigActor;
    if(ctx.children.has("server_config")){
        serverConfigActor = ctx.children.get("server_config");
    }

    const config = await query(serverConfigActor,  {}, global.actor_timeout);

    const user_id = authenticate_session.user_id;
    const scope = authenticate_session.scope;

    const now = new Date();
    const issued_at = now.getTime();

    const user = {
        _id: user_id
    }

    const key1 = await createClientKey(config);

    let it;
    if(scope === "openid"){
        //const claims = scopesToClaims(scope);
        const id_token = new IdTokenType("", key1);
        it= await id_token.createCryptToken(config, client, user);
    }

    const access_token = new AccessTokenType("", key1);
    const at = await access_token.createCryptToken(config, client, user);
    const expires_at = await access_token.expires_in();

    const refresh_token = new RefreshTokenType("", key1);
    const rt = await refresh_token.createCryptToken(config, client, user);

    return tokenResponse(at, expires_at, rt, scope, it);
}