import tokenResponse from "../response/tokenResponse";
import AccessTokenType from "../../../../token-types/AccessTokenType";
import RefreshTokenType from "../../../../token-types/RefreshTokenType";

const {ApolloError, AuthenticationError, ForbiddenError} = require('apollo-server');

export default async function(args, ctx) {

    let serverConfigActor;
    if(ctx.children.has("server_config")){
        serverConfigActor = ctx.children.get("server_config");
    }

    const config = await query(serverConfigActor,  {}, global.actor_timeout);

    const refreshToken = new RefreshTokenType(args.refresh_token);
    refreshToken.setKey(config.client_secret);

    await refreshToken.decrypt()

    const collectionItemActor = ctx.children.get("item");

    const client = await query(collectionItemActor, {"type": "client", search: {"_id": refreshToken.json.client_id} }, global.actor_timeout);

    const access_token = new AccessTokenType(config, client);
    const expires_at = access_token.expires_in();
    const refresh_token = RefreshTokenType(config, client);

    return tokenResponse(access_token, expires_at, refresh_token, refreshToken.scope);

}