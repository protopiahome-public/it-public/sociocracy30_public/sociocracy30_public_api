import IdTokenType from "../../../../token-types/IdTokenType";
import bcrypt from "bcrypt";

import {UnknownUserIDError, ExpiredTokenError, AccessDeniedError, InvalidUserCodeError} from "../../errors";

const { query } = require('nact');

//https://www.npmjs.com/package/jwt-pwd

export default async function(id_hint_token, key, ctx) {

    const collectionItemActor = ctx.children.get("item");

    const idToken = new IdTokenType(id_hint_token, key);
    await idToken.verify();
    const token = await idToken.getPayloadJSON();

    console.log("token:" + token);

    if(Date.now > token.exp ){
        throw new ExpiredTokenError("Token Expired");
    }

    let user;

    //azp vs acr
    switch (token.acr) {
        case "leaderid":
            user = await query(collectionItemActor, {type: "user", search: {"leader_id": token.sub}  }, global.actor_timeout);
            break;
        case "telegram":
            user = await query(collectionItemActor, {type: "user", search: {"telegram_id": Number.parseInt(token.sub)}  }, global.actor_timeout);
            break;
        case "vk":
            user = await query(collectionItemActor, {type: "user", search: {"vk_id": token.sub}  }, global.actor_timeout);
            break;
        case "slack":
            user = await query(collectionItemActor, {type: "user", search: {"slack_id": token.sub}  }, global.actor_timeout);
            break;
        case "email":
            user = await query(collectionItemActor, {type: "user", search: {"email": token.sub}  }, global.actor_timeout);
            break;
        default:
            break;
    }

    console.log(user);

    if (!user) {
        throw new UnknownUserIDError('No user')
    }

    return user;

}

//https://www.npmjs.com/package/jwt-otp