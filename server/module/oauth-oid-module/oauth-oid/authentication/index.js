import loginHint from "./loginHint";
import loginUserCode from "./loginUserCode";
import loginHintToken from "./loginHintToken";
import idHintToken from "./idHintToken";

module.exports = {

    idHintToken: idHintToken,
    loginHintToken: loginHintToken,
    loginUserCode: loginUserCode,
    loginHint: loginHint,

}
