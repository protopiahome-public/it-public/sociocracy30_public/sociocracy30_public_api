import LoginTokenType from "../../../../token-types/LoginTokenType";
import bcrypt from "bcrypt";

import {UnknownUserIDError, ExpiredTokenError, AccessDeniedError, InvalidUserCodeError} from "../../errors";

const { query } = require('nact');

//https://www.npmjs.com/package/jwt-pwd

export default async function(login_hint_token, user_code, key, ctx) {

    const collectionItemActor = ctx.children.get("item");

    const idToken = new LoginTokenType(login_hint_token, key); //id_token
    await idToken.verify();
    const id_token =  idToken.getPayloadJSON();

    console.log(user_code);
    console.log(id_token);

    if(Date.now > id_token.exp ){
        throw new ExpiredTokenError("Token Expired");
    }

    let user;


    switch (id_token.acr) {
        case "leaderid":
            user = await query(collectionItemActor, {type: "user", search: {"leader_id": id_token.sub}  }, global.actor_timeout);
            break;
        case "telegram":
            user = await query(collectionItemActor, {type: "user", search: {"telegram_id": id_token.sub}  }, global.actor_timeout);
            break;
        case "vk":
            user = await query(collectionItemActor, {type: "user", search: {"vk_id": id_token.sub}  }, global.actor_timeout);
            break;
        case "slack":
            user = await query(collectionItemActor, {type: "user", search: {"slack_id": id_token.sub}  }, global.actor_timeout);
            break;
        case "email":
            user = await query(collectionItemActor, {type: "user", search: {"email": id_token.sub}  }, global.actor_timeout);
            break;
        default:
            break;
    }

    let authenticator;
    switch (id_token.amr) {
        case "opt":
            authenticator =  await query(collectionItemActor, {"type": "associate_session", search: {"user_code": user_code, "for_chanel": id_token.acr} }, global.actor_timeout);
            // authenticator =  await query(collectionItemActor, {"type": "authenticate_session", search: {"user_code": user_code} }, global.actor_timeout);
            if (!authenticator){
                throw new InvalidUserCodeError('Incorrect user code')
            }

            switch (authenticator.for_channel) {
                case "slack":
                    user = await query(collectionItemActor, {type: "user", search:{slack_id: id_token.sub}  }, global.actor_timeout);
                    break;
                case "telegram":
                    user = await query(collectionItemActor, {type: "user", search:{telegram_id: id_token.sub}  }, global.actor_timeout);
                    break;
                case "vk":
                    user = await query(collectionItemActor, {type: "user", search:{vk_id: id_token.sub}  }, global.actor_timeout);
                    break;
            }
            break;
        case "mca":
            authenticator =  await query(collectionItemActor, {type: "associate_session", search: {"user_code": user_code} }, global.actor_timeout);
            if (authenticator){
                switch (authenticator.oob_channel) {
                    case "slack":
                        user = await query(collectionItemActor, {type: "user", search: {"_id": authenticator.user_id}, input:{slack_id: id_token.sub}  }, global.actor_timeout);
                        break;
                    case "telegram":
                        user = await query(collectionItemActor, {type: "user", search: {"_id": authenticator.user_id} , input:{telegram_id: id_token.sub}  }, global.actor_timeout);
                        break;
                    case "vk":
                        user = await query(collectionItemActor, {type: "user", search: {"_id": authenticator.user_id} , input:{vk_id: id_token.sub}  }, global.actor_timeout);
                        break;
                }
            } else{
                throw new InvalidUserCodeError('Incorrect user code')
            }
            break;
        case "pwd":
            const valid = bcrypt.compareSync(id_token.pwd, user.crypto_password);
            if (!valid) {
                throw new InvalidUserCodeError('Incorrect password')
            }
            break;
        default:
            break;
    }

    //search: {"_id": authenticator.user_id} ,

    if (!user) {
        throw new UnknownUserIDError('No user')
    }

    return user;

}

//https://www.npmjs.com/package/jwt-otp