import {UnknownUserIDError} from "../../errors";

const { query } = require('nact');

export default async function(user_code, client, ctx) {

    const collectionItemActor = ctx.children.get("item");

    let user;
    let authenticator;

    try {
        authenticator =  await query(collectionItemActor, {type: "associate_session", search: {"user_code": user_code} }, global.actor_timeout);

    }catch (e) {
        throw new UnknownUserIDError('Incorrect user code')
    }

    console.log(authenticator);

    // if(client.external_system === authenticator.for_channel){
        user = await query(collectionItemActor, {type: "user", search: {"_id": authenticator.user_id}  }, global.actor_timeout);
    // }else{
    //
    // }

    if(!user){
        throw new UnknownUserIDError('Not Exist')
    }


    return user;

}