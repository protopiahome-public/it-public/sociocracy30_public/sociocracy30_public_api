import JWTBearerGrantType from "../grant-types/JWTBearerGrantType";
import cibaPollGrantType from "../grant-types/cibaPollGrantType";
import deviceCodeGrantType from "../grant-types/deviceCodeGrantType";
import tokenDelegationGrantType from "../grant-types/tokenDelegationGrantType";
import refreshTokenGrantType from "../grant-types/refreshTokenGrantType";

import {InvalidGrantError, AccessDeniedError} from "../../errors";

export default async function(args, ctx) {

    const grant_type = args.grant_type;
    delete args.grant_type;

    // if(!client.grant_types.includes(grant_type))
    //     throw new AccessDeniedError('This client has not that grant type');

    let tokenResponse;

    switch (grant_type) {

        case "jwt-bearer":
            tokenResponse = JWTBearerGrantType(args, ctx);
            break;
        case "ciba":
            tokenResponse = cibaPollGrantType(args, ctx);
            break;
        case "device_code":
            tokenResponse = deviceCodeGrantType(args, ctx);
            break;
        case "token-exchange":
            tokenResponse = tokenDelegationGrantType(args, ctx);
            break;
        case "refresh_token":
            tokenResponse = refreshTokenGrantType(args, ctx);
            break;
        default:
            throw new InvalidGrantError('Invalid Grant');
    }

    return tokenResponse;
}

