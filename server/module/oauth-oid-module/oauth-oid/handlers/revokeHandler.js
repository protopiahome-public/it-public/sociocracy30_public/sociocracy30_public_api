const { query } = require('nact');
const {ApolloError, AuthenticationError, ForbiddenError} = require('apollo-server');

//отзывает токен
//black_list

export default async function(args, ctx) {

    const token = args.token;
    const token_type_hint = args.token_type_hint;

    const collectionItemActor = ctx.children.get("item");
    //token
    //token_type_hint
    if(token_type_hint === "access_token"){
        await  query(collectionItemActor, {type: "authenticate_session", search: {"access_token": token}, input: {"is_remove": true } }, global.actor_timeout);
    }else if(token_type_hint === "refresh_token"){
        await  query(collectionItemActor, {type: "authenticate_session", search: {"refresh_token": token}, input: {"is_remove": true } }, global.actor_timeout);
    }


    return true;




}

