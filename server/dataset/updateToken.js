// import AssertionTokenType from "../token-types/AssertionTokenType"
import TokenType from "../token-types/TokenType"
import db from "../db";
import {ObjectId} from "promised-mongo";
import createClientKey from "../token-types/createClientKey";
var jose = require('node-jose');


// const token = new AssertionTokenType();
// const key = TokenType.toKey();
// token.setKey(key);
// console.log(TokenType.asKey(key));
// token.sign();

async function main() {
	var clients = await db.client.find();
	for (var client of clients) {
		const key = await createClientKey(client);
		var key_secret = key.toJSON(true).k;
		await db.client.findAndModify({
                    query: {_id: new ObjectId(client._id)},
                    update: {
                        $set : {client_secret: key_secret}
                    }
                })
		console.log("kid: " + client._id + " ; k: " + key_secret);
	};
	console.log("secrets updated");
}

main();