import {ApolloServer} from "apollo-server-hapi";
const { dispatch, spawn, stop, query } = require('nact');
import {Server} from "hapi";
import appModule  from './modules';
import * as URI from "uri-js";
import AssertionTokenType from "./token-types/AssertionTokenType";
import TokenType from "./token-types/TokenType";

import userModule from "./module/user-module"
import db from "./db";
var jose = require('node-jose');

const reset = async (msg, error, ctx) => {
    if(error){
        console.log(error);
    }
    // return ctx.reset;
};

export default (parent) => spawn(
    parent,
    async (state, msg, ctx) =>
    {

        let serverConfigActor;
        if(ctx.children.has("server_config")){
            serverConfigActor = ctx.children.get("server_config");
        }

        const config = await query(serverConfigActor,  {}, global.actor_timeout);
        const {host, port} = URI.parse(config.uri);
        console.log("Welcome to Ecosystem");


        try {

            const { schema, context } = appModule(ctx);
            const apolloServer = new ApolloServer({
                schema,
                context,
                formatError: (err) => {
                    console.log(err);
                    return err;
                },

                introspection: true,
            });


            // const apolloServer = new ApolloServer(schema(ctx));

            //                host: state.uri,
            //                 port: state.port,

            const app = new Server({
                host: host,
                port: port,
                "routes": {
                    "cors": {
                        origin: ["*"],
                        headers: ["Accept", "Content-Type",  "id_token", "authorization"],
                        additionalHeaders: ["X-Requested-With"]
                    }
                }
            });

            //await
            await apolloServer.applyMiddleware({
                app,
            });

            //await
            await apolloServer.installSubscriptionHandlers(app.listener);


            //await
            await app.start();
        }catch (e) {
            console.log(e);
        }

        //config


    },
    "server",
    { onCrash: reset}
);

// throw new AuthorizationError('you must be logged in to query this schema');

//https://www.npmjs.com/package/role-acl
//https://onury.io/accesscontrol/?content=faq
//https://developer.mindsphere.io/concepts/concept-roles-scopes.html
//https://wso2.com/library/articles/2015/12/article-role-based-access-control-for-apis-exposed-via-wso2-api-manager-using-oauth-2.0-scopes/
//https://onury.io/accesscontrol/