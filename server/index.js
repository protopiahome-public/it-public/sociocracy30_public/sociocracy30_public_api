const path = require('path');
import {existsSync, readdirSync, readFileSync, statSync} from "fs";
const { dispatch } = require('nact');
import system from "./system";
import serverService from "./serverService";


global.actor_timeout = 2000;


const serverActor = serverService(system);
//TODO объявление всех акторов первого уровня должно быть здесь, а не в глубине когда отрабатывают dispatch,
// если только функция не вызывается множество раз
// swapLess и схожие модули объявляются здесь

// const actors1 = require("./module/main-module/actors/index.js");
// actors1.default(serverActor);

const dir = __dirname + "/module";
const files = readdirSync(dir);
for (const file of files) {
    const stat = statSync(path.join(dir, file));
    if (stat.isDirectory()){
        // console.log(path.join(dir, file));
        if (existsSync(path.join(dir, file, "/actors/index.js"))){
            const actors = require(path.join(dir, file, "/actors/index.js"));
            actors.default(serverActor)
        }
    }
}



// if(system.children.has("server")){
//     serverActor = system.children.get("server");
// } else {
//
//
// }

dispatch(serverActor, "");


//https://medium.com/@allistersmith/diving-into-event-driven-web-apps-with-node-js-and-nact-7ecbf19ed41f